/***********************************************************************
 * Module:  Film.java
 * Author:  p1909391
 * Purpose: Defines the Class Film
 ***********************************************************************/
//Pour extraire les données de la BDD, il aurait fallu passer par des classes
//Cependant je n'ai pas réussi et pas eu assez de temps, donc j'ai utilisé directement les données 
//La classe n'est donc pas utilisée, mais je l'ai laissée car il l'aurait fallue normalement...
package planningDesProjections.Controller;

import java.util.*;

public class Film {
   private int id;
   private String nom;

   public Film(int id, String nom) {
	   this.id = id;
	   this.nom = nom;
   }
   
   public int getId() {
	   return this.id;
   }
   
   public void setId(int id) {
	   this.id = id;
   }
   
   public String getNom() {
	   return this.nom;
   }
   
   public void setNom(String nom) {
	   this.nom = nom;
   }
}