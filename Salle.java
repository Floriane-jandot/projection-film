/***********************************************************************
 * Module:  Salle.java
 * Author:  p1909391
 * Purpose: Defines the Class Salle
 ***********************************************************************/

package planningDesProjections.Controller;
//Mêmes commentaires que pour Film.java
import java.util.*;

public class Salle {
   private int idSalle;
   private String nomSalle;
   private int nbPlaces;
   
   public Salle(int idSalle, String nomSalle, int nbPlaces) {
	   this.idSalle = idSalle;
	   this.nomSalle = nomSalle;
	   this.nbPlaces = nbPlaces;
   }
   
   public int getIdSalle() {
	   return this.idSalle;
   }
   
   public void setIdSalle(int idSalle) {
	   this.idSalle = idSalle;
   }
   
   public String getNomSalle() {
	   return this.nomSalle;
   }
   
   public void setNomSalle(String nomSalle) {
	   this.nomSalle = nomSalle;
   }
   
   public int getNbPlaces() {
	   return this.nbPlaces;
   }
   
   public void setNbPlaces(int nbPlaces) {
	   this.nbPlaces = nbPlaces;
   }

}