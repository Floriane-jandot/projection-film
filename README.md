Application de gestion de projections de films
Créée en 2020 avec java

L'application utilise une base de données de films afin de créer intelligemment un planning de projections pour un cinéma, puis l'affiche joliment à l'utilisateur.
