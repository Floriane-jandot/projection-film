package planningDesProjections.Controller;

import planningDesProjections.Model.BDDconnexion;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CréationPlanning {
    public static void test(javax.swing.JTable jTable1) throws SQLException{
    
                    ResultSet res = BDDconnexion.CM();
                    ResultSet salles = BDDconnexion.salles();
                    for (int i=0; i<jTable1.getRowCount(); i++) { //On initie la tableau à null
                	for (int j=1; j<jTable1.getColumnCount(); j++) {
                		jTable1.setValueAt(null, i, j);
                	}
                    }
                    String TableauSalles[] = new String[5]; //On place les noms des salles dans un tableau, utile par la suite
                    int i=0;
                    while(salles.next()){
                        String nomSalle = String.valueOf(salles.getString("Nom_salle"));
                        TableauSalles[i]=nomSalle;
                        i++;
                    }
                    while(res.next()){ //Tant qu'il y a des films
                        
                        String nom = String.valueOf(res.getString("Nom_film"));
                        int compteurY = 2 + (int)(Math.random() * ((10 - 2) + 2)); //On répartit les films au hasard entre les cases 2 et 10
                        boolean caseOccupee = true;
                        while (caseOccupee){
                            if ((jTable1.getValueAt(compteurY, 12)) == null){caseOccupee=false;} //On vérifie qu'il n'y a pas déjà un CM sur le créneau
                            else{
                                compteurY = 2 + (int)(Math.random() * ((10 - 2) + 2)); 
                            }
                        }
                        for(int k=0;k<4;k++){
                            String cellule = nom;
                            jTable1.setValueAt(cellule, compteurY+(k*14), 12); //jour 12 - On répète 4 fois les CM dans la même journée 
                        }                                                       //Il y a 10 CM et 4 quarts d'heures de pause entre chaque bloc, donc 10+4 = 14
                        
                    }
                    int numeroSalle = 0;   //placement des salles
                    int incrementeur = 2;  //les CM commencent à 9h donc bloc 2
                    while (incrementeur < 55){
                        if (incrementeur == 2 || incrementeur == 16 || incrementeur == 30 || incrementeur == 44){ //chaque début de "blocs" de CM
                            //On atrribue à tout ce même bloc une seule salle pour que les spectateurs n'aient pas à se déplacer
                            numeroSalle = (int) Math.round( Math.random() )  ; //Choix de la salle au hasard
                        }
                        String donnée = (String) jTable1.getValueAt(incrementeur, 12);
                        if (donnée!=null){ //Si il y a un CM, on lui donne sa salle
                            jTable1.setValueAt("<html><b>"+donnée+"</b><br>"+TableauSalles[numeroSalle]+"</html>", incrementeur, 12); 
                        }
                        incrementeur++;
                    }
                }
}

